from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.http import JsonResponse
from django.http import HttpResponse
import urllib.request, json
import requests
from .forms import FormStatus, SubscriberForm
from .models import FormStatusObj, Subscriber
from django.core.validators import validate_email
from django.core.exceptions import ValidationError
# gatauuuuuuuu
from django.core import serializers
from django.db import IntegrityError
from django.shortcuts import render
from django.contrib.auth import logout
from django.views.decorators.csrf import csrf_exempt

response={}
response['counter'] = 0
def index(request):
    context={'index':'active'}
    response['listform'] = FormStatus
    myform = FormStatusObj.objects.all()
    response['FormStatus'] = myform


    form = FormStatus(request.POST or None )
    if(request.method == 'POST' and form.is_valid()):
        response['status'] = request.POST['status']
        myform = FormStatusObj(status=response['status'])
        myform.save()
        return render(request, 'landing_page.html', response, context)
    else:
        return render(request, 'landing_page.html', response, context)

def profile(request):
    context={'profile':'active'}
    return render(request, 'profile.html', context)

def library(request):
    context={'library':'active'}
    return render(request, 'library.html', context)

def book_data(request, choice):
    raw_data = requests.get('https://www.googleapis.com/books/v1/volumes?q=' + choice).json()
    itemss =[] #list
    for info in raw_data['items']:
        data = {}
        data['cover'] = info['volumeInfo']['imageLinks']['thumbnail'] if 'imageLinks' in info['volumeInfo'].keys() else 'Tidak tersedia'
        data['title'] = info['volumeInfo']['title']
        data['author'] = ", ".join(info['volumeInfo']['authors']) if 'authors' in info['volumeInfo'].keys() else 'Tidak tersedia'
        data['publishedDate'] = info['volumeInfo']['publishedDate'] if 'publishedDate' in info['volumeInfo'].keys() else 'Tidak tersedia'
        itemss.append(data) #ditambahin ke itemss
    return JsonResponse({"data" :itemss}) #ngebalikin data

def subscribe(request):
    response = {
        'subscribe_form' : SubscriberForm
    }
    context={'subscribe':'active'}
    html = 'subscribe.html'
    return render(request, html, response)

def check_email(request):
    try:
        print(request.POST['email'])
        validate_email(request.POST['email'])
    except:
        return JsonResponse({
            'message':'Email format is wrong!',
            'status':'fail'
        })

    exist = Subscriber.objects.filter(email=request.POST['email'])

    if exist:
        return JsonResponse({
            'message':'ups, e-mail already exist',
            'status':'fail'
        })
    
    return JsonResponse({
        'message':'Email can be used',
        'status':'success'
    })

def save_subscriber(request):
    if (request.method == "POST"):
        subscriber = Subscriber(
            email = request.POST['email'],
            name = request.POST['name'],
            password = request.POST['password']   
        )
        subscriber.save()
        return JsonResponse({
            "message":'Subscribe success! We are friends now!'
        }, status = 200)
    else:
        return JsonResponse({
            "message":"There is no GET method here!"
        }, status = 403)

######## LIST SUBSCRIBER ########
def get_subscriber(request):
    subscribers = Subscriber.objects.all().values('name', 'email')  # or simply .values() to get all fields
    subscribers_list = list(subscribers)  # important: convert the QuerySet to a list object
    return JsonResponse(subscribers_list, safe=False)

def unsubscribe(request, email):
    Subscriber.objects.get(email=email).delete()
    return JsonResponse({})

#######SESSION-AN#########
def log_in(request):
	return render(request, 'login.html')

def log_out(request):
    request.session.flush()
    logout(request)
    return redirect('library')

@csrf_exempt
def add_counter(request):
	if request.user.is_authenticated:
		request.session['counter'] = request.session['counter'] + 1
	else :
		pass
	return HttpResponse(request.session['counter'], content_type= 'application/json')

@csrf_exempt
def min_counter(request):
	if request.user.is_authenticated:
		request.session['counter'] = request.session['counter'] - 1
	return HttpResponse(request.session['counter'], content_type= 'application/json')


def sign_user(request):
	if request.user.is_authenticated:
		request.session['username'] = request.user.username
		request.session['email'] = request.user.email
		if 'counter' not in request.session:
			request.session['counter'] = 0
		response['counter'] = request.session['counter']
	else:
		if 'counter' not in request.session:
			request.session['counter'] = 0
		response['counter'] = request.session['counter']

	return render(request, "library.html", response)


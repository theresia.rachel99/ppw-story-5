from django.db import models      

class FormStatusObj(models.Model):
    time = models.DateTimeField(auto_now_add=True)
    status = models.CharField(max_length=300)


class Subscriber(models.Model):
	name  = models.CharField(max_length = 60)
	email = models.EmailField(unique=True, db_index=True)
	password = models.CharField(max_length=30)

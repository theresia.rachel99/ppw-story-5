from django.urls import path
from .views import index, profile, library, book_data, subscribe, save_subscriber, check_email, get_subscriber, unsubscribe, log_in, log_out, add_counter, min_counter, sign_user
from django.conf.urls import url, include
# from django.contrib import admin
from django.contrib.auth import views

urlpatterns = [
    path('', index, name='index'),
    path('profile', profile, name='profile'),
    path('library', sign_user, name='library'),
    path('subscribe', subscribe, name='subscribe'),
    path('api/books/<str:choice>/', book_data, name='books'),
    path('save_subscriber/',save_subscriber, name='save_subscriber'),
    path('get_subscriber/',get_subscriber, name='get_subscriber'),
    path('unsubscribe/<str:email>/',unsubscribe, name='unsubscribe'),
    path('check_email/',check_email, name='check_email'),
    path('login/', log_in, name='login'),
    path('logout/', log_out, name='log_out'),
    path('add_counter/', add_counter, name='add_counter'),
    path('min_counter/', min_counter, name='min_counter'),
    path('sign_user/', sign_user, name='sign_user'),
    path('auth/', include('social_django.urls', namespace='social')),
]
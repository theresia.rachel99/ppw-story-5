from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index, profile, library, book_data, subscribe, sign_user, min_counter, add_counter, log_in, log_out
from .models import FormStatusObj, Subscriber
from .forms import FormStatus, SubscriberForm
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from .apps import App6Config
from django.apps import apps

    
class Lab6UnitTest(TestCase):
    def test_lab6_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    
    def test_lab6_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_lab6_contain_string(self):
        request = HttpRequest()
        response = index(request)
        string = response.content.decode("utf8")
        self.assertIn("Hello, Apa kabar?", string)

    def test_index_using_post(self):
        response_post = Client().post('/', {'status': 'saya belajar'})
        self.assertEqual(response_post.status_code, 200)
        self.assertTemplateUsed(response_post, 'landing_page.html')

    def test_lab6_using_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'landing_page.html')
    
    def test_model_can_create_news_status(self):
        # Creating a new status
        new_status = FormStatusObj.objects.create(status='saya makan')

        # Retrieving all available status
        counting_all_available_status = FormStatusObj.objects.all().count()
        self.assertEqual(counting_all_available_status, 1)

    #INI UNTUK PROFILE PAGE

    def test_profile_url_is_exist(self):
        response = Client().get('/profile')
        self.assertEqual(response.status_code, 200)
    
    def test_lab6_using_profile_func(self):
        found = resolve('/profile')
        self.assertEqual(found.func, profile)

    def test_lab6_using_profile_template(self):
        response = Client().get('/profile')
        self.assertTemplateUsed(response, 'profile.html')
    
    def test_lab6_contain_aboutMe(self):
        request = HttpRequest()
        response = profile(request)
        string = response.content.decode("utf8")
        self.assertIn("About Me", string)

    def test_lab6_contain_personal(self):
        request = HttpRequest()
        response = profile(request)
        string = response.content.decode("utf8")
        self.assertIn("Personal Details", string)

    def test_lab6_contain_title(self):
        request = HttpRequest()
        response = profile(request)
        string = response.content.decode("utf8")
        self.assertIn("PROFILE", string)

    def test_lab6_contain_quote(self):
        request = HttpRequest()
        response = profile(request)
        string = response.content.decode("utf8")
        self.assertIn('The better you know yourself, the better your relationship with the rest of the world', string)
    
    def test_lab6_contain_imgsrc(self):
        request = HttpRequest()
        response = profile(request)
        string = response.content.decode("utf8")
        self.assertIn('https://i.imgur.com/ldnL5TF.jpg', string)

    def test_lab9_apps(self):
        self.assertEqual(App6Config.name, 'app6')
        self.assertEqual(apps.get_app_config('app6').name, 'app6')


# class Lab7FunctionalTest(TestCase):

#     def setUp(self):
#         chrome_options = Options()
#         chrome_options.add_argument('--dns-prefetch-disable')
#         chrome_options.add_argument('--no-sandbox')
#         chrome_options.add_argument('--headless')
#         chrome_options.add_argument('disable-gpu')
#         service_log_path = "./chromedriver.log"
#         service_args = ['--verbose']
#         self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
#         super(Lab7FunctionalTest, self).setUp()

#     def tearDown(self):
#         self.selenium.implicitly_wait(5)
#         self.selenium.quit()
#         super(Lab7FunctionalTest, self).tearDown()
    
#     def test_input_status(self):
#         selenium = self.selenium
#         # Opening the link we want to test
#         selenium.get('http://rachel-larasati-story6.herokuapp.com/')
#         # find the form element
#         status = selenium.find_element_by_id('id_status')

#         submit = selenium.find_element_by_id('button')

#         # Fill the form with data
#         status.send_keys('Coba Coba')

#         # submitting the form
#         submit.send_keys(Keys.RETURN)
#         self.assertIn('Coba Coba', self.selenium.page_source)
    
#     def test_specified_title(self):
#         selenium  = self.selenium
#         selenium.get('http://rachel-larasati-story6.herokuapp.com/')

#         # check if the web contain title named "My status"
#         self.assertIn("My status", self.selenium.title)
    
#     def test_tag_form_in_body(self):
#         selenium  = self.selenium
#         selenium.get('http://rachel-larasati-story6.herokuapp.com/')
#         # find the tag form
#         form = selenium.find_element_by_tag_name('form')
#         #return true if the tag form is in the website
#         self.assertTrue(form, self.selenium.page_source)

#     def test_css_in_form(self):
#         selenium  = self.selenium
#         selenium.get('http://rachel-larasati-story6.herokuapp.com/')
#         #find the form result element
#         result = selenium.find_element_by_class_name('col-sm-4')
#         #find the css of the result
#         css_result = result.value_of_css_property('background-color')
#         #check the css
#         self.assertEqual('rgba(249, 235, 160, 1)', css_result)

#     def test_css_in_text_center(self):
#         selenium  = self.selenium
#         selenium.get('http://rachel-larasati-story6.herokuapp.com/')
#         #find the text center element
#         center = selenium.find_element_by_class_name('text-center')
#         #find the css of the text center
#         css_center = center.value_of_css_property('font-family')
#         #check the css
#         self.assertEqual('Rubik, sans-serif', css_center)

#     #INI UNTUK PROFILE PAGE
#     def test_specified_profile_title(self):
#         selenium  = self.selenium
#         selenium.get('http://rachel-larasati-story6.herokuapp.com/profile')

#         # check if the web contain title named "My status"
#         self.assertIn("Cerita.in", self.selenium.title)

#     # def test_changebutton_in_body_profile(self):
#     #     selenium  = self.selenium
#     #     selenium.get('http://rachel-larasati-story6.herokuapp.com/profile')
#     #     changebtn = selenium.find_element_by_class_name('btn-primary').textContent
#     #     self.assertEqual('Change Theme', changebtn)

#     def test_css_in_button(self):
#         selenium  = self.selenium
#         selenium.get('http://rachel-larasati-story6.herokuapp.com/profile')
#         #find the form result element
#         btn = selenium.find_element_by_class_name('btn-primary')
#         #find the css of the result
#         css_result = btn.value_of_css_property('background-color')
#         #check the css
#         self.assertEqual('rgba(51, 122, 183, 1)', css_result)
    
class Lab9UnitTest(TestCase): 
           
    def test_library_url_is_exist(self):
        response = Client().get('/library')
        self.assertEqual(response.status_code, 200)

    def test_library_using_book_data_func(self):
        found = resolve('/library')
        self.assertEqual(found.func, sign_user)

    def test_library_using_library_template(self):
        response = Client().get('/library')
        self.assertTemplateUsed(response, 'library.html')

    def test_success_when_not_added_before(self):
        response = Client().get('/api/books/comic/')
        self.assertEqual(response.status_code, 200)
        self.assertJSONNotEqual(
            str(response.content, encoding='utf8'),{''}
        )

class Lab10UnitTest(TestCase): 
           
    def test_subscribe_url_is_exist(self):
        response = Client().get('/subscribe')
        self.assertEqual(response.status_code, 200)

    def test_subscribe_using_book_data_func(self):
        found = resolve('/subscribe')
        self.assertEqual(found.func, subscribe)

    def test_subscribe_using_library_template(self):
        response = Client().get('/subscribe')
        self.assertTemplateUsed(response, 'subscribe.html')

    
    # def test_check_email_already_exist(self):
    #     Subscriber.objects.create(email="bisakok@gmail.com", name="bisakok", password="ppwppw")
    #     response = Client().post('/check_email', data={"email": "bisakok@gmail.com"})
    #     self.assertEqual(response.json()['status'], 'fail')

    # def test_subscribe_using_post(self):
    #     response_post = Client().post('/save_subscriber', {'name': 'Rachel', 'email':'theresia@gmail.com', 'password':'888888888'})
    #     self.assertEqual(response_post.status_code, 301)
    #     print(response_post.content)
    #     self.assertJSONEqual(
    #         str(response_post.content, encoding='utf8'),{''}
    #     )
    
    # def test_subscribe_using_get(self):
    #     response_get = Client().get('/save_subscriber', {'name': 'Rachel', 'email':'theresia@gmail.com', 'password':'888888888'})
    #     self.assertEqual(response_get.status_code, 301)
    #     self.assertJSONEqual(
    #         str(response_get.content, encoding='utf8'),{
    #         "message":"There is no GET method here!"}
    #     )


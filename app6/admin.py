from django.contrib import admin
from .models import FormStatusObj, Subscriber

admin.site.register(FormStatusObj)
admin.site.register(Subscriber)
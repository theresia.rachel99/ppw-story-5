$( function() {
    $( "#accordion" ).accordion({
      heightStyle: "content", collapsible: true
    });
  } );

$(document).ready(function(theme){
    var clicked = true;
    $(".apply-button").click(function(theme){
        if(clicked == true){ 
            $(".judul").css({"color": "#F9EBA0", "background-color": "black"});
            $("#profile h4").css({"font-family": ("Rubik", "sans-serif")});
            document.body.style.backgroundImage = "url('http://3.bp.blogspot.com/-EVZBZIK-pkA/VLLBFfbP17I/AAAAAAAABvo/oiCDtbpHAoE/s1600/Yellow-wallpaper-2.jpg')";
            clicked=false;
        } else {
            $(".judul").css({"color": "#D46A6A", "background-color": "black"});
            $("#profile h4").css({"font-family": ("Handlee", "cursive")});
            document.body.style.backgroundImage = "url('https://i.imgur.com/8vlvztE.jpg')";
            clicked=true;
        }
    });
});

document.onreadystatechange = function(e)
{
  if(document.readyState=="interactive")
  {
    var all = document.getElementsByTagName("*");
    for (var i=0, max=all.length; i < max; i++) 
    {
      set_ele(all[i]);
    }
  }
}

function check_element(ele)
{
  var all = document.getElementsByTagName("*");
  var totalele=all.length;
  var per_inc=100/all.length;

  if($(ele).on())
  {
    var prog_width=per_inc+Number(document.getElementById("progress_width").value);
    document.getElementById("progress_width").value=prog_width;
    $("#bar1").animate({width:prog_width+"%"},10,function(){
      if(document.getElementById("bar1").style.width=="100%")
      {
        $(".progress").fadeOut("slow");
      }			
    });
  }

  else	
  {
    set_ele(ele);
  }
}

function set_ele(set_element)
{
  check_element(set_element);
}


var counter =0;
var urlku = '';
$(document).ready(function() { //dijalanin kalau document udh siap ngeload
    $('.pilih').click(function(){
        urlku = $(this).attr('id');
        $.ajax({
            type : 'GET',
            url : urlku, //manggil url yg isinya json
            dataType : 'json',
            success : function(data) { //succes klo urlnya bener dan data yang keambil typenya json jalanin. data itu yg diterima
                var print ='<tr>'; //ngeprint tr karna bikinnya row
                console.log(data.data[0].title);//buat ngecek di htmlnya, inspect elemen lalu pilih console
                    for(var i=1; i<=data.data.length; i++) {
                        print+= '<th scope ="row">' + i+'</th> <td>';
                        print += '<img src=' + data.data[i-1].cover + '>' + '</td> <td>';
                        print += data.data[i-1].title+'</td> <td>';
                        print+= data.data[i-1].author;
                        print+= '</td> <td>' + data.data[i-1].publishedDate+'</td> <td>';
                        print+='<a href="#" id="star" class="fa fa-star"></a></td></tr>';
                    }
                    $('#tableku tbody').empty();
                    $('tbody').append(print); //nge print si print di tbody
            }
        });
    });
    $(document).on('click', '#star', function() { //kalo di klik starnya jalanin
        if( $(this).hasClass('clicked') ) { //kalo udh punya class click makanya di kurang dan class click nya dihapus
            $(this).removeClass('clicked');
            $.ajax({
                url : "/min_counter",
                dataType : 'json',
                success : function(result){
                    $('.counters').html(result); 
                    // var counter = JSON.parse(result);
                }
            })
        }
        else {//kalo belum punya di tambah
            $(this).addClass('clicked');
            $.ajax({
                url : "/add_counter",
                dataType : 'json',
                success : function(result){
                        $('.counters').html(result); 
                }
            })

        }
    });

});

//////////STORY 10///////////
var emailIsValid = false;

$(document).ready(function() {
    $("input").focusout(function() {
        checkAll();
    });

    $("#email-form").keyup(function() {
        checkEmail();
    });

    $("#pass-form").keyup(function() {
        $('#statusForm').html('');
        if ($('#pass-form').val().length < 8) {
            $('#statusForm').append('<small style="color: red"> Password at least 8 character </small>');
        }
        checkAll();
    });

    $('input').focusout(function() {
        checkEmail()
    });

    $('#subs-button').click(function () {
        data = {
            'email' : $('#email-form').val(),
            'name' : $('#name-form').val(),
            'password' : $('#pass-form').val(),
            "csrfmiddlewaretoken": document.getElementsByName('csrfmiddlewaretoken')[0].value,
        }
        $.ajax({
            type : 'POST',
            url : 'save_subscriber/',
            data : data,
            dataType : 'json',
            success : function(data) {
                alert(data['message']);
                document.getElementById('email-form').value = '';
                document.getElementById('name-form').value = '';
                document.getElementById('pass-form').value = '';
                $('#statusForm').html('');
                $('#subs-button').prop('disabled', true);
            }
        })
    });
})



function checkEmail() {
    data = {
        'email':$('#email-form').val(),
        "csrfmiddlewaretoken": document.getElementsByName('csrfmiddlewaretoken')[0].value,
    }
    $.ajax({
        type: "POST",
        url: 'check_email/',
        data: data,
        dataType: 'json',
        success: function(data) {
            $('#statusForm').html('');
            if (data['status'] === 'fail') {
                emailIsValid = false;
                $('#subs-button').prop('disabled', true);
                $('#statusForm').append('<small style="color:red">' + data["message"] + '</small>');
            } else {
                emailIsValid = true;
                checkAll();
                $('#statusForm').append('<small style="color:green">' + data["message"] + '</small>');
            }
            
        }
    });
}

function checkAll() {
    if (emailIsValid && 
        $('#name-form').val() !== '' && 
        $('#pass-form').val() !== '' &&
        $('#pass-form').val().length > 7) {
        
        $('#subs-button').prop('disabled', false);
    } else {
        $('#subs-button').prop('disabled', true);
    }
}

///////////// CHALLENGE 10 /////////////////////////
$(document).ready(function() { //dijalanin kalau document udh siap ngeload
    $.ajax({
        type : 'GET',
        url : "get_subscriber/", //manggil url yg isinya json
        dataType : 'json',
        success : function(data) { //succes klo urlnya bener dan data yang keambil typenya json jalanin. data itu yg diterima
            var print =''; //ngeprint tr karna bikinnya row
                for(var i=1; i<=data.length; i++) {
                    print+= '<tr id="'+data[i-1].email+'" ><td scope ="row">' + data[i-1].name+'</td><td>';
                    print+= data[i-1].email+'</td> <td>';
                    print+=' <button type="button" onclick=unsub("'+ data[i-1].email +'") class="btn btn-outline-light subbtn hapus"> unsubscribe </button> </td></tr>';
                }
                $('.subclass').append(print); //nge print si print di tbody
        }
    });
});

function unsub(email){
    $.ajax({
        type : 'GET',
        url : "unsubscribe/" + email,
        dataType : 'json',
        success : function(data) { //succes klo urlnya bener dan data yang keambil typenya json jalanin. data itu yg diterima
            document.getElementById(email).remove()
        }
    });
}

//////// SESSION//////////


